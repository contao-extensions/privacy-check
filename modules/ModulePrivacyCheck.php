<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2022 Leo Feyer
 *
 * @package   privacy-check
 * @author    Fast & Media | Christian Schmidt <dev@fast-media.net>
 * @license   LGPL
 * @copyright Fast & Media 2022 <https://www.fast-media.net>
 */


/**
 * Run in a custom namespace, so the class can be replaced
 */
namespace PrivacyCheck;


class ModulePrivacyCheck extends \BackendModule
{

	/**
	 * Template
	 * @var string
	 */
	protected $strTemplate = 'be_pricacy_check';

  /**
   * Implement the commands to run by this batch program
   */
  public function compile()
  {
		global $arrExecuteTypes;

		$this->Template->version = 'Version 1.2.4';
		$this->Template->info = $GLOBALS['TL_LANG']['privacy_check']['info'];

		\System::loadLanguageFile('tl_layout');

		if(\Input::post('step') == 'deactivate')
		{
			\Database::getInstance()->prepare("UPDATE tl_layout SET webfonts_temp = webfonts, webfonts = '' WHERE webfonts != ''")->execute();
		}

    // Prevent errors with Contao >= 4.11
    $strSelect = '';
    $arrColums = array();
		$arrExecuteVars = array();

		// Contao 4.10 or lower
		if (version_compare(VERSION . '.' . BUILD, '4.11.0', '<'))
		{
			if (\Database::getInstance()->fieldExists('webfonts', 'tl_layout'))
			{
				$strSelect = ', webfonts, jSource, mooSource';
				$arrColums[] = "webfonts != '' OR jSource = ? OR jSource = ? OR mooSource = ? OR mooSource = ?";

				$arrExecuteVars = array('j_googleapis', 'j_fallback', 'moo_googleapis', 'moo_fallback');

				// Count layouts with webfonts
				$intCountLayouts = \Database::getInstance()->prepare("SELECT COUNT(*) AS count FROM tl_layout WHERE webfonts != ''")->execute()->count;
				if($intCountLayouts)
				{
					$this->Template->warning_webfonts = sprintf($GLOBALS['TL_LANG']['privacy_check']['layout']['warning_webfonts'], $intCountLayouts);
				}
			}
		}

		// Define all problematic urls/phrases in head/script area @ layouts + all templates
		$arrExecuteTypes = array
		(
			'%googleadservices.com%',
			'%googleapis.com%',
			'%googletagmanager.com%',
			'%google-analytics.com%',
			'%typekit.net%',
			'%bootstrapcdn.com%',
			'%jquery.com%',
			'%cloudfront.com%',
			'%cookiebot.com%',
			'%@import%',
			'%src="https://%',
			'%src="http://%',
			'%src="//%'
		);

		for($i=1; $i<=count($arrExecuteTypes); $i++)
		{
			$arrColums[] = 'script LIKE ?';
		}

		for($i=1; $i<=count($arrExecuteTypes); $i++)
		{
			$arrColums[] = 'head LIKE ?';
		}

    if($arrColums)
    {
      $strColums = ' WHERE ' . implode(' OR ', $arrColums);
    }

		$arrExecuteVars = array_merge($arrExecuteVars, $arrExecuteTypes, $arrExecuteTypes);

		// Count layouts with problems in general
    $objLayouts = \Database::getInstance()->prepare("SELECT id, name, script, head" . $strSelect . " FROM tl_layout" . $strColums)->execute($arrExecuteVars);
		$intCountLayouts = $objLayouts->numRows;

		$this->Template->layout_warning = sprintf($GLOBALS['TL_LANG']['privacy_check']['layout']['warning'], $intCountLayouts);

		$arrLayouts = array();

		while($objLayouts->next())
		{
			$arrNotes = array();

			if($objLayouts->jSource && $objLayouts->jSource != 'j_local')
			{
				$arrNotes[] = $GLOBALS['TL_LANG']['privacy_check']['layout']['jquery'];
			}

			if($objLayouts->mooSource && $objLayouts->mooSource != 'moo_local')
			{
				$arrNotes[] = $GLOBALS['TL_LANG']['privacy_check']['layout']['mootools'];
			}

			foreach($arrExecuteTypes AS $strType)
			{
				$strType = str_replace('%', '', $strType);

				if(stristr($objLayouts->script, $strType))
				{
					$arrNotes[] = sprintf($GLOBALS['TL_LANG']['privacy_check']['layout']['script'], $strType);
				}
				
				if(stristr($objLayouts->head, $strType))
				{
					$arrNotes[] = sprintf($GLOBALS['TL_LANG']['privacy_check']['layout']['head'], $strType);
				}
			}
		
			$objPages = \Database::getInstance()->prepare("SELECT id, title, includeLayout FROM tl_page WHERE layout = ? ORDER BY title ASC")->execute($objLayouts->id);
			$intCountPages = $objPages->numRows;

			$arrPages = array();
			while($objPages->next())
			{
				if($objPages->includeLayout)
				{
					$arrPages[] = array
					(
						'id' => $objPages->id,
						'title' => $objPages->title
					);
				}
			}

			$arrLayouts[] = array
			(
				'id' => $objLayouts->id,
				'name' => $objLayouts->name,
				'edit' => sprintf($GLOBALS['TL_LANG']['tl_layout']['edit'], $objLayouts->id),
				'show' => sprintf($GLOBALS['TL_LANG']['tl_layout']['show'], $objLayouts->id),
				'notes' => $arrNotes,
				'webfonts' => $objLayouts->webfonts,
				'pages' => $arrPages
			);

			$this->Template->webfonts_info = $GLOBALS['TL_LANG']['privacy_check']['webfonts']['info'];
		}

		if($arrLayouts)
		{
			$this->Template->layouts = $arrLayouts;
		}
		else
		{
			$this->Template->layout_success = $GLOBALS['TL_LANG']['privacy_check']['layout']['success'];
			$this->Template->layout_info = $GLOBALS['TL_LANG']['privacy_check']['layout']['info'];
		}

		$this->Template->labels = $GLOBALS['TL_LANG']['privacy_check'];

		// Check templates
		$stack = [\Contao\System::getContainer()->getParameter('kernel.project_dir') . '/templates'];
		$arrTemplates = $this->readDir($stack);

		if($arrTemplates)
		{
			$this->Template->template_warning = sprintf($GLOBALS['TL_LANG']['privacy_check']['template']['warning'], count($arrTemplates));
			$this->Template->templates = $arrTemplates;
		}
		else
		{
			$this->Template->template_success = $GLOBALS['TL_LANG']['privacy_check']['template']['success'];
			$this->Template->template_info = $GLOBALS['TL_LANG']['privacy_check']['template']['info'];
		}

		// Check pages
		if (\Database::getInstance()->fieldExists('ga_analyticsid', 'tl_page'))
		{
			// Count root pages with problems
			$objPages = \Database::getInstance()->prepare("SELECT id, title, includeLayout, ga_analyticsid FROM tl_page WHERE ga_analyticsid!=''")->execute();
			$intCountPages = $objPages->numRows;

			if($intCountPages)
			{
				$this->Template->page_warning = sprintf($GLOBALS['TL_LANG']['privacy_check']['page']['warning'], $intCountPages);
			}

			$arrPages = array();

			while($objPages->next())
			{
				$arrNotes = array();

				$arrNotes[] = sprintf($GLOBALS['TL_LANG']['privacy_check']['page']['analytics'], $objPages->ga_analyticsid);

				$arrPages[] = array
				(
					'id' => $objPages->id,
					'title' => $objPages->title,
					'notes' => $arrNotes
				);
			}

			if($arrPages)
			{
				$this->Template->pages = $arrPages;
			}
			else
			{
				$this->Template->page_success = $GLOBALS['TL_LANG']['privacy_check']['page']['success'];
				$this->Template->page_info = $GLOBALS['TL_LANG']['privacy_check']['page']['info'];
			}
		}
	}

	protected function readDir($stack, $folder='') 
	{
		global $arrExecuteTypes;

		while (count($stack) > 0)
		{
			$path = array_pop($stack);
		}

		$objFiles = @opendir($path);
    $arrFiles = array();
		$arrTemplates = array();

		while ($file = readdir($objFiles))
		{
			// Check other folders
			if (!stristr($file, '.'))
			{
				$stack = [\Contao\System::getContainer()->getParameter('kernel.project_dir') . '/templates/' . $file];
				$arrTemplates = $this->readDir($stack, $file);
			}
			elseif (strpos($file, 'fe_') === 0)
			{
				$arrNotes = array();

				$arrFiles[] = $file;

				$strFileContent = file_get_contents($path . '/' . $file);

				foreach($arrExecuteTypes AS $strType)
				{
					$strType = str_replace('%', '', $strType);
	
					if(stristr($strFileContent, $strType))
					{
						$arrNotes[] = sprintf($GLOBALS['TL_LANG']['privacy_check']['template']['notes'], $strType);
					}
				}

				if($arrNotes)
				{
					if($folder)
					{
						$strUrl = $folder . '/' . $file;
					}
					else
					{
						$strUrl = $file;
					}

					$arrTemplates[] = array
					(
						'folder' => $folder,
						'url' => $strUrl,
						'name' => $file,
						'notes' => $arrNotes,
						//'layouts' => $arrLayouts
					);
				}
			}
		}

    closedir($objFiles);
    sort($arrFiles);

		return $arrTemplates;
	}
}
