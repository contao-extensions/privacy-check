<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2022 Leo Feyer
 *
 * @package   privacy-check
 * @author    Fast & Media | Christian Schmidt <dev@fast-media.net>
 * @license   LGPL
 * @copyright Fast & Media 2022 <https://www.fast-media.net>
 */

// Contao 4.10 or lower
if (version_compare(VERSION . '.' . BUILD, '4.11.0', '<'))
{
	$strTable = 'tl_layout';

	$GLOBALS['TL_DCA'][$strTable]['palettes']['default'] = str_replace(',webfonts', ',webfonts,webfonts_temp', $GLOBALS['TL_DCA'][$strTable]['palettes']['default']);

	/**
	 * Add fields to tl_layout
	 */
	$GLOBALS['TL_DCA'][$strTable]['fields']['webfonts_temp'] = array
	(
		'label'							=> &$GLOBALS['TL_LANG'][$strTable]['webfonts_temp'],
		'exclude'						=> true,
		'inputType'					=> 'text',
		'eval'							=> array('maxlength'=>255, 'tl_class'=>'w50'),
		'sql'								=> "varchar(255) NOT NULL default ''"
	);

	$GLOBALS['TL_DCA'][$strTable]['fields']['webfonts']['eval']['disabled'] = true;

	if(\Input::get('enabled'))
	{
		$GLOBALS['TL_DCA'][$strTable]['fields']['webfonts']['eval']['disabled'] = false;
	}
}
