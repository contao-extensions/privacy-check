<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2022 Leo Feyer
 *
 * @package   privacy-check
 * @author    Fast & Media | Christian Schmidt <dev@fast-media.net>
 * @license   LGPL
 * @copyright Fast & Media 2022 <https://www.fast-media.net>
 */

/**
 * Miscellaneous
 */
$GLOBALS['TL_LANG']['privacy_check']['extension']['current'] = array('Aktuelle Infos zur Erweiterung', 'Mehr erfahren auf fast-media.net');
$GLOBALS['TL_LANG']['privacy_check']['extension']['help'] = array('Hilfe & Support', 'Schnelle Hilfe auf fast-media.net');

$GLOBALS['TL_LANG']['privacy_check']['info'] = '<span>Wichtiger Hinweis:</span> Diese Hinweise und die Erweiterung insgesamt stellen keine rechtsverbindliche Auskunft dar.';

$GLOBALS['TL_LANG']['privacy_check']['layout']['headline'] = 'Seitenlayouts';
$GLOBALS['TL_LANG']['privacy_check']['layout']['layout'] = 'Seitenlayout';

$GLOBALS['TL_LANG']['privacy_check']['layout']['warning'] = 'Problematische Seitenlayouts insgesamt: %s';
$GLOBALS['TL_LANG']['privacy_check']['layout']['warning_webfonts'] = 'Problematische Seitenlayouts mit Google Webfonts: %s';

$GLOBALS['TL_LANG']['privacy_check']['layout']['title'] = 'Layout-Titel';
$GLOBALS['TL_LANG']['privacy_check']['layout']['problems'] = 'Mögliche Datenschutzprobleme';
$GLOBALS['TL_LANG']['privacy_check']['layout']['affected_pages'] = 'Betroffene Seiten';
$GLOBALS['TL_LANG']['privacy_check']['layout']['pages'] = 'Seiten';

$GLOBALS['TL_LANG']['privacy_check']['layout']['jquery'] = '<strong>jQuery-Quelle:</strong> CDN statt "Lokale Datei" gewählt';
$GLOBALS['TL_LANG']['privacy_check']['layout']['mootools'] = '<strong>MooTools-Quelle:</strong> CDN statt "Lokale Datei" gewählt';
$GLOBALS['TL_LANG']['privacy_check']['layout']['head'] = '<strong>Eigene Head-Tags:</strong> %s eingebunden';
$GLOBALS['TL_LANG']['privacy_check']['layout']['script'] = '<strong>Eigener JavaScript-Code:</strong> %s eingebunden';

$GLOBALS['TL_LANG']['privacy_check']['layout']['success'] = 'In keinem Ihrer Seitenlayouts scheinen externe Scripte oder Webfonts integriert zu sein.';

$GLOBALS['TL_LANG']['privacy_check']['layout']['info'] = '<strong>Wichtiger Hinweis:</strong> Es werden nur die Seitenlayouts im Backend geprüft. Zudem werden dabei nur die Felder "Google Webfonts", "Eigener JavaScript-Code" sowie "Eigene Head-Tags" geprüft. Der eigentliche Quellcode im Frontend wird nicht direkt geprüft.';

$GLOBALS['TL_LANG']['privacy_check']['webfonts']['info'] = '<strong>Jetzt Optimierung durchführen:</strong> Sie können durch den Klick auf den folgenden Button die Webfonts in allen Seitenlayouts gleichzeitig deaktivieren. Dies hat eine Datenbank-Änderung zur Folge. Sie können aber nach der Aktion weiterhin die bisher verwendeten Google Fonts sehen und auch auf dieser Seite bearbeiten. Klicken Sie dazu auf eines der Seitenlayouts. Sollten Sie keine Fehler mehr in Ihren Seitenlayouts haben, aber trotzdem die Google Webfonts ändern möchten, tragen Sie bitte in die dcaconfig folgendes ein:';
$GLOBALS['TL_LANG']['privacy_check']['webfonts']['submit'] = array('Webfonts in allen Seitenlayouts deaktivieren', 'Diese Einstellung setzt die Webfonts in den Seitenlayout in ein deaktiviertes Feld.');


$GLOBALS['TL_LANG']['privacy_check']['template']['headline'] = 'Seiten-Templates';

$GLOBALS['TL_LANG']['privacy_check']['template']['warning'] = 'Problematische Templates: %s';
$GLOBALS['TL_LANG']['privacy_check']['template']['notes'] = '<strong>%s</strong> direkt im Seiten-Template eingebunden';

$GLOBALS['TL_LANG']['privacy_check']['template']['success'] = 'In keinem Ihrer Seiten-Templates (fe_page z.B.) scheinen externe Scripte oder Webfonts direkt integriert zu sein.';

$GLOBALS['TL_LANG']['privacy_check']['template']['info'] = '<strong>Wichtiger Hinweis:</strong> Es werden nur die fe_ Templates geprüft. Es ist möglich, dass rechtswidrige Scripte über andere Wege eingebunden sind, z.B. mittels Iframes oder anderer Templates.';


$GLOBALS['TL_LANG']['privacy_check']['page']['headline'] = 'Seitenstruktur';
$GLOBALS['TL_LANG']['privacy_check']['page']['warning'] = 'Problematische Startpunkte in der Seitenstruktur: %s';
$GLOBALS['TL_LANG']['privacy_check']['page']['info'] = '<span>Wichtiger Hinweis:</span> Der Startpunkt wird nur darauf geprüft, ob die Erweiterung hofff/googlenalaytics installiert ist. Es ist möglich, dass rechtswidrige Scripte über andere Wege eingebunden sind.';
$GLOBALS['TL_LANG']['privacy_check']['page']['success'] = 'In keinem Ihrer Startpunkte scheint Google Analytics direkt eingebunden zu sein.';

$GLOBALS['TL_LANG']['privacy_check']['page']['analytics'] = '<strong>ID für Google Analytics:</strong> Feld darf nicht ausgefüllt sein -  %s';
