<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2022 Leo Feyer
 *
 * @package   privacy-check
 * @author    Fast & Media | Christian Schmidt <dev@fast-media.net>
 * @license   LGPL
 * @copyright Fast & Media 2022 <https://www.fast-media.net>
 */

$strTable = 'tl_layout';

/**
 * Fields
 */
$GLOBALS['TL_LANG'][$strTable]['webfonts_temp'] = array('Google-Webfonts (Info)', 'Dieses Feld dient lediglich der Information, welche Schriftart(en) in diesem Seitenlayout zuvor eingetragen wurde(n).');
