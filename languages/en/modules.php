<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2022 Leo Feyer
 *
 * @package   privacy-check
 * @author    Fast & Media | Christian Schmidt <dev@fast-media.net>
 * @license   LGPL
 * @copyright Fast & Media 2022 <https://www.fast-media.net>
 */
 
$GLOBALS['TL_LANG']['MOD']['privacy_check'] = array('Datenschutz-Check', 'Hier können Sie einige mögliche Datenschutz-Probleme in Ihrer Contao Installation prüfen.');
