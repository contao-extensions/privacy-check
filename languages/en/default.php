<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2022 Leo Feyer
 *
 * @package   privacy-check
 * @author    Fast & Media | Christian Schmidt <dev@fast-media.net>
 * @license   LGPL
 * @copyright Fast & Media 2022 <https://www.fast-media.net>
 */

/**
 * Miscellaneous
 */
$GLOBALS['TL_LANG']['privacy_check']['extension']['current'] = array('Current informations about this extension', 'Learn more at fast-media.net');
$GLOBALS['TL_LANG']['privacy_check']['extension']['help'] = array('Help & Support', 'Fast help on fast-media.net');

$GLOBALS['TL_LANG']['privacy_check']['info'] = '<span>Important note:</span> These notes and the extension overall do not represent or support legally binding information.';

$GLOBALS['TL_LANG']['privacy_check']['layout']['headline'] = 'Page layouts';
$GLOBALS['TL_LANG']['privacy_check']['layout']['layout'] = 'Page layout';

$GLOBALS['TL_LANG']['privacy_check']['layout']['warning'] = 'Problematic page layouts overall: %s';
$GLOBALS['TL_LANG']['privacy_check']['layout']['warning_webfonts'] = 'Problematic page layouts with Google web fonts: %s';

$GLOBALS['TL_LANG']['privacy_check']['layout']['title'] = 'Layout title';
$GLOBALS['TL_LANG']['privacy_check']['layout']['problems'] = 'Possible privacy issues';
$GLOBALS['TL_LANG']['privacy_check']['layout']['affected_pages'] = 'Affected pages';
$GLOBALS['TL_LANG']['privacy_check']['layout']['pages'] = 'Pages';

$GLOBALS['TL_LANG']['privacy_check']['layout']['jquery'] = '<strong>jQuery source:</strong> CDN selected instead of "Local file".';
$GLOBALS['TL_LANG']['privacy_check']['layout']['mootools'] = '<strong>MooTools source:</strong> CDN selected instead of "Local file"';
$GLOBALS['TL_LANG']['privacy_check']['layout']['head'] = '<strong>Additional head tags:</strong> %s integrated';
$GLOBALS['TL_LANG']['privacy_check']['layout']['script'] = '<strong>Custom JavaScript code:</strong> %s integrated';

$GLOBALS['TL_LANG']['privacy_check']['layout']['success'] = 'There don\'t appear to be any external scripts or web fonts integrated in any of your page layouts.';

$GLOBALS['TL_LANG']['privacy_check']['layout']['info'] = '<strong>Important note:</strong> Only the page layouts in the backend are checked. In addition, only the fields "Google web fonts", "Custom JavaScript code" and "Additional head tags" are checked. The actual source code in the frontend is not checked directly.';

$GLOBALS['TL_LANG']['privacy_check']['webfonts']['info'] = '<strong>Perform optimization now:</strong> You can deactivate the web fonts in all page layouts at the same time by clicking on the following button. This results in a database change. However, after the campaign you can still see the google web fonts used so far and edit them via this page here. To do this, click on one of the page layouts. If you no longer have errors in your page layouts, however want to enter a Google web font again, please enter the following in the dcaconfig:';

$GLOBALS['TL_LANG']['privacy_check']['webfonts']['submit'] = array('Disable web fonts in all page layouts', 'Diese Einstellung setzt die Web fonts in den Seitenlayout in ein deaktiviertes Feld.');


$GLOBALS['TL_LANG']['privacy_check']['template']['headline'] = 'Page template';

$GLOBALS['TL_LANG']['privacy_check']['template']['warning'] = 'Problematic Templates: %s';
$GLOBALS['TL_LANG']['privacy_check']['template']['notes'] = '<strong>%s</strong> directly integrated in the page template';

$GLOBALS['TL_LANG']['privacy_check']['template']['success'] = 'None of your page templates (fe_page for example) seem to have any external scripts or web fonts directly built in.';

$GLOBALS['TL_LANG']['privacy_check']['template']['info'] = '<strong>Important note:</strong> Only the fe_ templates are checked. It is possible that illegal scripts are integrated via other means, e.g. using iframes or other templates.';


$GLOBALS['TL_LANG']['privacy_check']['page']['headline'] = 'Site structure';
$GLOBALS['TL_LANG']['privacy_check']['page']['warning'] = 'Problematic website roots in the Site structure: %s';
$GLOBALS['TL_LANG']['privacy_check']['page']['info'] = '<span>Important note:</span> The starting point is only checked if the hoff/googlenalaytics extension is installed. It is possible that illegal scripts are integrated via other ways.';
$GLOBALS['TL_LANG']['privacy_check']['page']['success'] = 'Google Analytics doesn\'t seem to be directly integrated into any of your starting points.';

$GLOBALS['TL_LANG']['privacy_check']['page']['analytics'] = '<strong>ID for Google Analytics:</strong> Field must be empty - %s';
