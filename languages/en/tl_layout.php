<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2022 Leo Feyer
 *
 * @package   privacy-check
 * @author    Fast & Media | Christian Schmidt <dev@fast-media.net>
 * @license   LGPL
 * @copyright Fast & Media 2022 <https://www.fast-media.net>
 */

$strTable = 'tl_layout';

/**
 * Fields
 */
$GLOBALS['TL_LANG'][$strTable]['webfonts_temp'] = array('Google web fonts (Info)', 'This field only provides information about which font(s) was (were) previously entered in this page layout.');
