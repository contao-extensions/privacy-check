<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2022 Leo Feyer
 *
 * @package   privacy-check
 * @author    Fast & Media | Christian Schmidt <dev@fast-media.net.net>
 * @license   LGPL
 * @copyright Fast & Media 2022 <https://www.fast-media.net>
 */

/**
 * Back end modules
 */
$GLOBALS['BE_MOD']['system']['privacy_check'] = array
(
	'tables'						=> array('tl_layout'),
	'table'							=> array('TableWizard', 'importTable'),
	'callback'					=> 'PrivacyCheck\ModulePrivacyCheck'
);
