<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2022 Leo Feyer
 *
 * @package   privacy-check
 * @author    Fast & Media | Christian Schmidt <dev@fast-media.net>
 * @license   LGPL
 * @copyright Fast & Media 2022 <https://www.fast-media.net>
 */


/**
 * Register the namespaces
 */
ClassLoader::addNamespaces(array
(
	'PrivacyCheck',
));


/**
 * Register the classes
 */
ClassLoader::addClasses(array
(
	'PrivacyCheck\ModulePrivacyCheck'     => 'system/modules/privacy-check/modules/ModulePrivacyCheck.php'
));


/**
 * Register the templates
 */
TemplateLoader::addFiles(array
(
	'be_pricacy_check' => 'system/modules/eventim_import/templates',
));
