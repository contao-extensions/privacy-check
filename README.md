# Contao Datenschutz Check Erweiterung
Die Contao Erweiterung prüft nach Installation alle Seitenlayouts einer Contao Installation, ob Google Fonts oder Cloud-Dienste wie jQuery.com, bootstrap.com oder Google Analytics etc. eingebunden sind.

## Vollständige Anleitung und Support verfügbar unter:
https://www.fast-media.net/de/produkte/contao-erweiterung/datenschutz-check/

# Kompatibilität
Die Version 1.2.0 der Erweiterung ist kompatibel mit Contao 4.4.0 bis 4.12.x.
Vermutlich funktioniert die Erweiterung auch mit Contao 4.13 (sobald erschienen).

Definitiv getestet wurden folgende Versionen:

* Contao 4.4 LTS
* Contao 4.9 LTS (diverse Installationen)
* Contao 4.12 
* Contao 4.13 LTS

## Features
* Prüfung der Google Fonts im Seitenlayout (bei Contao 4.11 und 4.12 wird das nur im Head- und Script-Bereich geprüft).
* Prüfung von CDN / Cloud-Diensten in Scripten und im Headbereich
* Übersichtliche Struktur und Verwaltung im Backend
* Hindert Redakteure daran Google Fonts in das dafür vorgesehene Feld ins Seitenlayout einzutragen (nicht bei Contao 4.11 und 4.12).
* Verringert Risiken, da die dynamische Einbindung von Google Webfonts rechtswidrig ist (und wahrscheinlich auch andere US-Dienste)

## Kurzerklärung
Die Erweiterung bringt erweiterte Backend-Funktionalitäten mit sich. Nach der Installation der Erweiterung im Contao Manager, ändern sich zwei Dinge im Contao Backend:

* Es erscheint ein neuer Backend-Navigationspunkt unter dem Bereich "System"
* Das Feld "Google Webfonts" wird in allen Seitenlayouts deaktiviert (nicht bei Contao 4.11 und 4.12).

![Backend Ansicht](https://www.fast-media.net/assets/images/6/backend-datenschutz-seitenlayouts-fa28b724.webp)

## Vollständige Anleitung und weitere Bilder
https://www.fast-media.net/de/produkte/contao-erweiterung/datenschutz-check/